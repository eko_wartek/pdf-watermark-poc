package main

import (
	"log"

	"github.com/pdfcpu/pdfcpu/pkg/api"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
)

func main() {
	wm, err := pdfcpu.ParseTextWatermarkDetails("Ini Watermark Bro", "sc:.9, d:2, c:.6 .2 .9", true, pdfcpu.CENTIMETRES)
	if err != nil {
		log.Println(err)
	}
	if err = api.AddWatermarksFile("testPDF_Version.6.x.pdf", "out.pdf", nil, wm, nil); err != nil {
		log.Println(err)
	}
}
